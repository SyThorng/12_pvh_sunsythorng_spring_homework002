package com.example.spring_pvh_sunsythorng_hw002.repository;

import com.example.spring_pvh_sunsythorng_hw002.model.Customer;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.CustomerRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Results(id = "mapCustomer", value = {
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "customerName", column = "customer_name"),
            @Result(property = "customerAddress", column = "customer_address"),
            @Result(property = "customerPhone", column = "customer_phone"),
    })
    @Select("SELECT * FROM customer_tb")
    List<Customer> getAllCustomer();


    @Select("Delete From customer_tb where customer_id=#{id} returning *")
    @ResultMap("mapCustomer")
    Customer deleteCustomer(int id);

    @Select("SELECT * from customer_tb where customer_id=#{id}")
    @ResultMap("mapCustomer")
    Customer searchByIdCustomer(int id);

    @Select("UPDATE customer_tb set customer_name=#{customerRequest.customerName},customer_address=#{customerRequest.customerAddress},customer_phone=#{customerRequest.customerPhone} where customer_id=#{id} returning *;")
    @ResultMap("mapCustomer")
    Customer updateCustomer(CustomerRequest customerRequest, int id);

    @Select("insert into customer_tb (customer_name, customer_address, customer_phone) values (#{customerRequest.customerName},#{customerRequest.customerAddress},#{customerRequest.customerPhone}) returning *;")
    @ResultMap("mapCustomer")
    Customer insertCustomer(@Param("customerRequest") CustomerRequest customerRequest);
}
