package com.example.spring_pvh_sunsythorng_hw002.repository;

import com.example.spring_pvh_sunsythorng_hw002.model.Invoice;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.InvoiceRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.Respon;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRespository {
    @Select("select * from invoice_detail join product_tb pt on pt.product_id = invoice_detail.product_id where invoice_id=#{id}")
    @Result(property = "productID", column = "product_id")
    @Result(property = "productName", column = "product_name")
    @Result(property = "productPrice", column = "product_price")
    List<Product> getProductByInvoiceId(Integer id);


    @Select("SELECT * FROM invoice_tb")
    @Results(id = "mapInvoice", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id", one = @One(select = "com.example.spring_pvh_sunsythorng_hw002.repository.CustomerRepository.searchByIdCustomer")),
            @Result(property = "products", column = "invoice_id", many = @Many(select = "getProductByInvoiceId")),
    })
    List<Invoice> getAllInvoice();


    @Select("select * from invoice_tb where invoice_id=#{id}")
    @ResultMap("mapInvoice")
    Invoice searchByIdInvcoice(int id);


    @Delete("delete from invoice_tb where invoice_id=#{id}")
    int deleteInvoice(int id);

    @Select("update invoice_tb set customer_id=#{invoiceRequest.customer} where invoice_id=#{id} returning invoice_id")
    Integer updateInvoice(InvoiceRequest invoiceRequest, int id);

    @Delete("delete from invoice_detail where invoice_id=#{id}")
    void deleteInvoiceformupdateInvoicve(int id);

    @Select("insert into invoice_tb(invoice_date,customer_id)values(#{invoiceRequest.invoiceDate},#{invoiceRequest.customer})" +
            "returning invoice_id")
    Integer addNewInvoice(@Param("invoiceRequest") InvoiceRequest invoiceRequest);

    @Select("insert into invoice_detail(product_id,invoice_id)values(#{pro_id},#{id})")
    void addInvoiceDetail(Integer id, Integer pro_id);


}
