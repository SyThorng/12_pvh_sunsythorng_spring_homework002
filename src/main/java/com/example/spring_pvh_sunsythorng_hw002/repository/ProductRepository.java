package com.example.spring_pvh_sunsythorng_hw002.repository;

import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {


    @Results(id = "mapProduct", value = {
            @Result(property = "productID", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    })
    @Select("SELECT * FROM product_tb")
    List<Product> getAllProduct();


    @Select("SELECT * from product_tb where product_id=#{id}")
    @ResultMap("mapProduct")
    Product searchById(int id);

    @Select("Delete From product_tb where product_id=#{id} returning *")
    @ResultMap("mapProduct")
    Product deleteProduct(int id);

    @Select("UPDATE product_tb set product_name=#{productRequest.productName},product_price=#{productRequest.productPrice} where product_id=#{id} returning *")
    @ResultMap("mapProduct")
    Product updatePruduct(ProductRequest productRequest, int id);



    @Select("INSERT INTO product_tb (product_name, product_price) VALUES (#{productRequest.productName},#{productRequest.productPrice}) returning *")
    @ResultMap("mapProduct")
    Product insertNewProduct(@Param("productRequest") ProductRequest productRequest);

//    @Select("Select * from product_tb where ")
//    Product getallProduct(Integer id);
}
