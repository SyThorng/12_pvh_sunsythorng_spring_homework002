package com.example.spring_pvh_sunsythorng_hw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPvhSunsythorngHw002Application {
    public static void main(String[] args) {
        SpringApplication.run(SpringPvhSunsythorngHw002Application.class, args);
    }

}
