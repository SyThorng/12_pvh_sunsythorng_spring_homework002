package com.example.spring_pvh_sunsythorng_hw002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    private Integer invoiceId;
    private LocalDate invoiceDate;
    private Customer customer;
    private List<Product> products;

}
