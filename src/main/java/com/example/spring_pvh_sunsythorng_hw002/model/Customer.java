package com.example.spring_pvh_sunsythorng_hw002.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    private int customerId;
    private String customerName;

    private String customerAddress;
    private String customerPhone;
}
