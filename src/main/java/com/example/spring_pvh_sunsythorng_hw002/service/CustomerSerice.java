package com.example.spring_pvh_sunsythorng_hw002.service;

import com.example.spring_pvh_sunsythorng_hw002.model.Customer;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.CustomerRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;

import java.util.List;

public interface CustomerSerice {
    List<Customer> getAllCustomer();

    Customer searchByIdCustomer(int id);

    Customer deleteByIDCustomer(int id);

    Customer updateByIDCustomer(CustomerRequest customerRequest, int id);

    Customer insertNewCustomer(CustomerRequest customerRequest);


}
