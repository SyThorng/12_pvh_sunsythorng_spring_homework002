package com.example.spring_pvh_sunsythorng_hw002.service.impl;

import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.repository.ProductRepository;
import com.example.spring_pvh_sunsythorng_hw002.service.ProductService;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImplement implements ProductService {

    private final ProductRepository productRepository;

    public ProductImplement(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product searchById(int id) {
        return productRepository.searchById(id);
    }

    @Override
    public Product deleteByID(int id) {
        return productRepository.deleteProduct(id);
    }

    @Override
    public Product updateByID(ProductRequest productRequest, int id) {
        return productRepository.updatePruduct(productRequest, id);
    }

    @Override
    public Product insertNewProduct(ProductRequest productRequest) {

        return productRepository.insertNewProduct(productRequest);
    }


}
