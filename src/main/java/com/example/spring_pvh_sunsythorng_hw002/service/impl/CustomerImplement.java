package com.example.spring_pvh_sunsythorng_hw002.service.impl;

import com.example.spring_pvh_sunsythorng_hw002.model.Customer;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.repository.CustomerRepository;
import com.example.spring_pvh_sunsythorng_hw002.service.CustomerSerice;
import com.example.spring_pvh_sunsythorng_hw002.ui.CustomerRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerImplement implements CustomerSerice {


    private final CustomerRepository customerRepository;

    public CustomerImplement(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer searchByIdCustomer(int id) {
        return customerRepository.searchByIdCustomer(id);
    }

    @Override
    public Customer deleteByIDCustomer(int id) {
        return customerRepository.deleteCustomer(id);
    }

    @Override
    public Customer updateByIDCustomer(CustomerRequest customerRequest, int id) {
        return customerRepository.updateCustomer(customerRequest, id);
    }

    @Override
    public Customer insertNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertCustomer(customerRequest);
    }

}
