package com.example.spring_pvh_sunsythorng_hw002.service.impl;

import com.example.spring_pvh_sunsythorng_hw002.model.Invoice;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.repository.InvoiceRespository;
import com.example.spring_pvh_sunsythorng_hw002.service.InvoiceService;
import com.example.spring_pvh_sunsythorng_hw002.ui.InvoiceRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceImplement implements InvoiceService {
    private final InvoiceRespository invoiceRespository;

    public InvoiceImplement(InvoiceRespository invoiceRespository) {
        this.invoiceRespository = invoiceRespository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRespository.getAllInvoice();
    }

    @Override
    public Invoice searchByIdInvoice(int id) {
        return invoiceRespository.searchByIdInvcoice(id);
    }

    @Override
    public int deleteInvoice(int id) {
        return invoiceRespository.deleteInvoice(id);
    }


    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, int id) {
        int id_ivoice = invoiceRespository.updateInvoice(invoiceRequest, id);
        invoiceRespository.deleteInvoiceformupdateInvoicve(id_ivoice);
        for (int i = 0; i < invoiceRequest.getProducts().size(); i++) {
            invoiceRespository.addInvoiceDetail(id_ivoice, invoiceRequest.getProducts().get(i));
        }
        return id_ivoice;
    }

    @Override
    public int addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer id = invoiceRespository.addNewInvoice(invoiceRequest);
        for (int i = 0; i < invoiceRequest.getProducts().size(); i++) {
            invoiceRespository.addInvoiceDetail(id, invoiceRequest.getProducts().get(i));
        }
        return id;
    }


}
