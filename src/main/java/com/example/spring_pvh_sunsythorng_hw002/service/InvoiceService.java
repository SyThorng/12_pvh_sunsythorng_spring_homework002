package com.example.spring_pvh_sunsythorng_hw002.service;

import com.example.spring_pvh_sunsythorng_hw002.model.Invoice;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice searchByIdInvoice(int id);

    int deleteInvoice(int id);

    Integer updateInvoice(InvoiceRequest invoiceRequest, int id);


    int addNewInvoice(InvoiceRequest invoiceRequest);
}
