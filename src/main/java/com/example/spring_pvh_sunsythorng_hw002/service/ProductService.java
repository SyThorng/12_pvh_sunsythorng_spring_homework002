package com.example.spring_pvh_sunsythorng_hw002.service;

import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product searchById(int id);

    Product deleteByID(int id);

    Product updateByID(ProductRequest productRequest, int id);

    Product insertNewProduct(ProductRequest productRequest);


}
