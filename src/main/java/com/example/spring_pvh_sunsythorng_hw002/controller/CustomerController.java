package com.example.spring_pvh_sunsythorng_hw002.controller;

import com.example.spring_pvh_sunsythorng_hw002.model.Customer;
import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.service.CustomerSerice;
import com.example.spring_pvh_sunsythorng_hw002.ui.CustomerRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.Respon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerSerice customerSerice;

    public CustomerController(CustomerSerice customerSerice) {
        this.customerSerice = customerSerice;
    }

    @GetMapping("/get-All-Customer")
    public ResponseEntity<Respon<List<Customer>>> getAllProduct() {
        Respon<List<Customer>> respon = Respon.<List<Customer>>builder()
                .payload(customerSerice.getAllCustomer()).message("Get all Customer Successfully..!!!")
                .success(true).build();
        return ResponseEntity.ok().body(respon);
    }

    @DeleteMapping(path = "/delete-customer/{id}")
    public ResponseEntity<Respon> serrchDeleteProduct(@PathVariable int id) {
        if (customerSerice.deleteByIDCustomer(id) != null) {
            Respon<Customer> respon = Respon.<Customer>builder()
                    .payload(customerSerice.deleteByIDCustomer(id)).message("Delete Customer successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<Respon> sechCustomerById(@PathVariable int id) {
        if (customerSerice.searchByIdCustomer(id) != null) {
            Respon<Customer> respon = Respon.<Customer>builder()
                    .payload(customerSerice.searchByIdCustomer(id)).message("Data Found...!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/update-Customer-by-id/{id}")
    public ResponseEntity<Respon> updateCustomer(@PathVariable int id, @RequestBody CustomerRequest customerRequest) {
        if (customerSerice.searchByIdCustomer(id) != null) {
            Respon<Customer> respon = Respon.<Customer>builder()
                    .payload(customerSerice.updateByIDCustomer(customerRequest, id)).message("Update Customer successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/add-new-customer")
    public ResponseEntity<Respon> addNewProduct(@RequestBody CustomerRequest customerRequest) {
        Respon<Customer> respon = Respon.<Customer>builder()
                .payload(customerSerice.insertNewCustomer(customerRequest)).message("Insert Successfully")
                .success(true).build();
        return ResponseEntity.ok().body(respon);
    }


}

