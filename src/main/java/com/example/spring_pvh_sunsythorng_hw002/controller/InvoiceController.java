package com.example.spring_pvh_sunsythorng_hw002.controller;

import com.example.spring_pvh_sunsythorng_hw002.model.Customer;
import com.example.spring_pvh_sunsythorng_hw002.model.Invoice;
import com.example.spring_pvh_sunsythorng_hw002.service.InvoiceService;
import com.example.spring_pvh_sunsythorng_hw002.ui.InvoiceRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.Respon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;


    @GetMapping("/getallInvoice")
    public ResponseEntity<Respon<List<Invoice>>> getAllInvoice() {
        Respon<List<Invoice>> response = Respon.<List<Invoice>>builder()
                .payload(invoiceService.getAllInvoice())
                .message("Get all invoice Successfully..!!")
                .success(true).build();
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("insert/invoice")
    public ResponseEntity<Respon<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest) {
        int id = invoiceService.addNewInvoice(invoiceRequest);
        Invoice invoice = invoiceService.searchByIdInvoice(id);
        Respon<Invoice> respon = Respon.<Invoice>builder()
                .payload(invoice)
                .message("Data Found..!!")
                .success(true).build();

        return ResponseEntity.ok().body(respon);
    }

    @GetMapping("/seachByIdInoive/{id}")
    public ResponseEntity<Invoice> searchByIdInvoice(@PathVariable int id) {
        if (invoiceService.searchByIdInvoice(id) != null) {
            Respon<Invoice> respon = Respon.<Invoice>builder()
                    .payload(invoiceService.searchByIdInvoice(id))
                    .message("Data Found..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon.getPayload());
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @DeleteMapping("/deleteInvoice/{id}")
    public ResponseEntity<Respon> deleteInvoice(@PathVariable int id) {
        if (invoiceService.deleteInvoice(id) == 1) {
            Respon<Invoice> respon = Respon.<Invoice>builder()
                    .payload(null)
                    .message("Delete Successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updateInvoice/{id}")
    public ResponseEntity<Respon> updateInvoice(@PathVariable int id, @RequestBody InvoiceRequest invoiceRequest) {
        int id_up = invoiceService.updateInvoice(invoiceRequest, id);
        Invoice invoice = invoiceService.searchByIdInvoice(id_up);
        if (invoice != null) {
            Respon<Invoice> respon = Respon.<Invoice>builder()
                    .payload(invoice)
                    .message("Updated Successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        }
        return ResponseEntity.notFound().build();
    }

}
