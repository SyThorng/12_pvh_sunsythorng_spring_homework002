package com.example.spring_pvh_sunsythorng_hw002.controller;


import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import com.example.spring_pvh_sunsythorng_hw002.service.ProductService;
import com.example.spring_pvh_sunsythorng_hw002.ui.ProductRequest;
import com.example.spring_pvh_sunsythorng_hw002.ui.Respon;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-All-Product")
    public ResponseEntity<Respon<List<Product>>> getAllProduct() {
        Respon<List<Product>> respon = Respon.<List<Product>>builder()
                .payload(productService.getAllProduct()).message("Get all product Successfully..!!!")
                .success(true).build();
        return ResponseEntity.ok().body(respon);
    }


    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<Respon> serchProductById(@PathVariable int id) {
        if (productService.searchById(id) != null) {
            Respon<Product> respon = Respon.<Product>builder()
                    .payload(productService.searchById(id)).message("Data Found...!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping(path = "/delete-product-by-id/{id}")
    public ResponseEntity<Respon> serrchDeleteProduct(@PathVariable int id) {
        if (productService.searchById(id) != null) {
            Respon<Product> respon = Respon.<Product>builder()
                    .payload(productService.deleteByID(id)).message("Delete Product successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping(path = "/update-product-by-id/{id}")
    public ResponseEntity<Respon> serrchDeleteProduct(@PathVariable int id, @RequestBody ProductRequest productRequest) {
        if (productService.searchById(id) != null) {
            Respon<Product> respon = Respon.<Product>builder()
                    .payload(productService.updateByID(productRequest, id)).message("Update Product successfully..!!")
                    .success(true).build();
            return ResponseEntity.ok().body(respon);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/add-new-product")
    public ResponseEntity<Respon> addNewProduct(@RequestBody ProductRequest productRequest) {
        Respon<Product> respon = Respon.<Product>builder()
                .payload(productService.insertNewProduct(productRequest)).message("Insert Successfully")
                .success(true).build();
        return ResponseEntity.ok().body(respon);
    }

}
