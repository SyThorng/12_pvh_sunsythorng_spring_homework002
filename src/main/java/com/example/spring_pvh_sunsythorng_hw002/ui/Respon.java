package com.example.spring_pvh_sunsythorng_hw002.ui;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder

public class Respon<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    T payload;
    String message;
    Boolean success;
}
