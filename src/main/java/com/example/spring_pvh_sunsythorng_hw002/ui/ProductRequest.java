package com.example.spring_pvh_sunsythorng_hw002.ui;

import com.example.spring_pvh_sunsythorng_hw002.model.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    private String productName;
    private double productPrice;


}
