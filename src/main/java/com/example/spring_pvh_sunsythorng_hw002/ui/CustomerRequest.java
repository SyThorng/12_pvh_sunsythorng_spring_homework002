package com.example.spring_pvh_sunsythorng_hw002.ui;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {
    private String customerName;

    private String customerAddress;
    private String customerPhone;
}
